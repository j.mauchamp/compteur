import {StyleSheet} from "react-native";
import Counter from "./Counter";
import { store } from "./store";
import { Provider } from "react-redux";

export default function App() {
  return (
    <Provider store={store} styles={styles.container}>
      <Counter/>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#121331",
    alignItems: "center",
    justifyContent: "center",
  },
});
