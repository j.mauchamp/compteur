import { View, Text, SafeAreaView, TouchableOpacity } from "react-native";
import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { decrement, increment } from "./counterSlice";
import {StyleSheet} from "react-native";

export default function Counter() {
  const count = useSelector((state) => state.counter.value);
  const dispatch = useDispatch();

  return (
    <SafeAreaView style={styles.container}>
        <Text style={styles.size2}>Compteur</Text>
    <View style={styles.plusmoins}>
    <View style={styles.moins}>
        <TouchableOpacity
        onPress={() => {
            dispatch(decrement());
        }}
        >
        <Text style={styles.size}>-</Text>
        </TouchableOpacity>
    </View>
    <View style={styles.incre}>
        <Text style={styles.size2}>{count}</Text>
    </View>
    <View style={styles.plus}>
        <TouchableOpacity
        onPress={() => {
            dispatch(increment());
        }}
        >
        <Text style={styles.size}>+</Text>
        </TouchableOpacity>
    </View>
    </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: "grey"
    },
    size: {
        fontSize: 50,
        color: "white"
    },
    size2: {
        fontSize: 50,
    },
    plusmoins: {
        flexDirection: "row",
        backgroundColor: "beige",
        padding: 50,
        borderWidth: 3,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    moins: {
        fontSize: 50,
        backgroundColor: "red",
        marginRight: 10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,

    }, 
    plus: {
        backgroundColor: "green",
        marginLeft: 10,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    },
    incre: {
        color: "red",
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
    }
  });
  